import { useContext, useState, useEffect } from 'react';
import courseData from '../../data/coursesdata.js';
import CourseCard from '../../components/CourseCard';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import UserContext from '../../UserContext';
import Head from 'next/head';
import Router from 'next/router';

export default function index({ data }) {
	const { user } = useContext(UserContext);

	//states for edit form iput
	const [courseName, setCourseName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	//states for course Id to be edited
	const [courseId, setCourseId] = useState('');

	//state for showing and closing the modal edit form
	const [showForm, setShowForm] = useState(false);

	//state for storing JWT
	const [token, setToken] = useState('');

	//sets token from localstorage
	useEffect(() => {
		setToken(localStorage.getItem('token'))
	})

	const handleClose = () => setShowForm(false);
	const handleShow = (courseId) => {

		fetch(`http://localhost:4000/api/courses/${courseId}`)
		.then(res => {
			return res.json()
		})
		.then(data => {
			setCourseId(data._id)
			setCourseName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setShowForm(true);
		})
	}

	function editCourse(e) {
		e.preventDefault();

		fetch('http://localhost:4000/api/courses', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				name: courseName,
				description: description,
				price: price
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			//if update course successful
			if(data === true){
				Router.push('/courses')
				setShowForm(false)
			}else{
				//error in editing page
				Router.push('/errors/1')
				setShowForm(false)
			}
		})
	}

	function disable(courseId) {
		fetch(`http://localhost:4000/api/courses/${courseId}`, {
			method: 'DELETE',
			headers: {
				'Authorization': `Bearer ${token}`
			}
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			//if archive course successful
			if(data === true){
				Router.push('/courses')
			}else{
				//error in archiving course
				Router.push('/errors/1')
			}
		})
	}

	const courses = data.map(indivCourse => {
		if(indivCourse.isActive){
			return (
				<CourseCard 
					key={indivCourse._id} 
					courseProp={indivCourse}
				/>
			)
		} else {
			return null;
		};
	})

	const courseRows = data.map(indivCourse => {
		return (
			<tr key={indivCourse._id}>
				<td>{indivCourse.name}</td>
				<td>Php {indivCourse.price}</td>
				<td>{indivCourse.isActive ? 'open' : 'closed'}</td>
				<td>
					<Button className="bg-warning" onClick={() => {handleShow(indivCourse._id)}}>Update</Button>
					<Button className="bg-danger" onClick={() => {disable(indivCourse._id)}}>Disable</Button>
				</td>
			</tr>
		)
	})

	return (
		user.isAdmin === true
		?
		<React.Fragment>


			<Head>
				<title>Courses Admin Dashboard</title>
			</Head>


			<h1>Course Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>		
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ courseRows }
				</tbody>
			</Table>

			<Modal show={showForm} onHIde={handleClose}>
			    <Modal.Header closeButton>
			        <Modal.Title>Modal heading</Modal.Title>
			    </Modal.Header>
			    <Modal.Body>
			        <Form onSubmit={(e) => editCourse(e)}>
			            <Form.Group controlId="courseName">
			                <Form.Label>Course Name:</Form.Label>
			                <Form.Control 
			                    type="text" 
			                    placeholder="Enter course name" 
			                    value={courseName} 
			                    onChange={e => {setCourseName(e.target.value)}} 
			                    required
			                />
			            </Form.Group>
			            <Form.Group controlId="description">
			                <Form.Label>Course Description:</Form.Label>
			                <Form.Control 
			                    as="textarea" 
			                    rows="3" 
			                    placeholder="Enter course description" 
			                    value={description} 
			                    onChange={e => {setDescription(e.target.value)}} 
			                    required
			                />
			            </Form.Group>
			            <Form.Group controlId="price">
			                <Form.Label>Course Price:</Form.Label>
			                <Form.Control 
			                    type="number" 
			                    value={price} 
			                    onChange={e => {setPrice(e.target.value)}} 
			                    required
			                />
			            </Form.Group>

			            <Button className="bg-primary" type="submit">Submit</Button>
			        </Form>
			    </Modal.Body>
			    <Modal.Footer>
			        <Button className="bg-secondary" onClick={handleClose}>
			            Close
			        </Button>
			    </Modal.Footer>
			</Modal>

		</React.Fragment>
		:
		<React.Fragment>
			<Head>
				<title>Courses Index</title>
			</Head>
			{courses}
		</React.Fragment>
	)
}

//getServerSideProps - called on every request
//user request 
export async function getServerSideProps() {
	//fetch data from endpoint
	//await - wait to fetch data before proceed to next step
	const res = await fetch('http://localhost:4000/api/courses')
	const data = await res.json()

	//return the props
	return {
		props: {
			data
		}
	}
}

// localhost:4000 => run/serve => getStaticProps => availableData during running
// localhost:4000 => run/serve => getServerSideProps => currentData